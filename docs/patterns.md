# Patterns

# Forms

Use the [Formik](https://github.com/jaredpalmer/formik) library when creating forms in web-frontend. Formik removes a lot
of the verbosity of creating forms in React.

In addition, [yup](https://github.com/jquense/yup) should be used for creating form validations. Yup has build in validations
for a lot of common use cases, and new validations can be built on top of its existing validations.

Forms are typed to a model representing the data being collected in the form.

Example form

```
import {Field, FieldProps, Form, Formik, FormikActions, FormikProps} from 'formik';
import * as Yup from 'yup';

// form model interface
export interface ApplicantDetails {
  firstName: string;
  lastName: string;
  identificationType: 'driversLicense' | 'passport';
  applicantConsent: boolean;
  ...
}

<Formik
              initialValues={this.state.applicantDetails as ApplicantDetails}
              onSubmit={(
                values: ApplicantDetails,
                actions: FormikActions<ApplicantDetails>
              ) => this.onSubmit(values, actions)}
              validationSchema={ApplicantDetailsSchema}
              render={({
                errors,
                touched,
                isSubmitting,
                handleChange,
                setFieldValue,
                handleBlur,
                submitCount,
                values,
              }: FormikProps<ApplicantDetails>) => ()
              />
```

See the [api docs](https://jaredpalmer.com/formik/docs/api/formik) on the methods available in the render prop

## Field validations

Formik supports a validation schema from Yup. Providing one will perform validations automatically.

Example schema:

```
import * as Yup from 'yup';

const ApplicantDetailsSchema = Yup.object().shape({
  firstName: Yup.string().required('First name is required'),
  lastName: Yup.string().required('Last name is required'),
  dateOfBirth: Yup.string()
    .matches(/^\d{1,2}\/\d{1,2}\/\d{4}$/, 'DOB must be in DD/MM/YYY format')
    .required('Date of birth is required'),
    })
```

# Routing

[React Router](https://reacttraining.com/react-router/web/guides/philosophy) is used for client side routing.

Related routes should be grouped in one file. For example routes under `${properties_id}/tenant/screening/*`
would be defined in one file with a `<Switch>`

```
<Switch>
  <Route
        path={'${properties_id}/tenant/screening/intro'}
        exact={true}
        render={(props) => (
             <TenantVettingIntroduction
                propertyId={props.match.params.propertyId}
                {...props}
              />
            )}
          />
  <Redirect from={'${properties_id}/tenant/screening'} to={'${properties_id}/tenant/screening/intro'} />
</Switch>
```

Include a `<Redirect>` route as a catch for when no routes match the given route pattern.

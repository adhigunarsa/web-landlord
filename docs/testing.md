# Testing

Unit tests are run with [Jest](https://facebook.github.io/jest/)

Tests are created next to the file they are testing and named using the pattern: `<file-being-tested>.spec.ts`

eg the tests for `currency.ts` are in `currency.spec.ts`

# Running Tests

Run `npm test` to start Jest in interactive watch mode. Jest will run the tests that are related to the last commit. When
files are changed, Jest will run the tests that are related to those files.

While in watch mode, press `a` to run all tests.

# Debugging Tests

Run `npm run test:debug` to start Jest with a debugger attached. Then open `chrome://inspect` in Google Chrome.

Click the inspect link to open a chrome dev tools window that will be attached to the node process. Click resume in the
debugger to start the tests.

Once the tests run once, files can be opened, and debug points can be placed like normal.

**_Note_** This requires Node >=8

# Test APIs

Jest uses Jasmine internally, so many of the Jasmine APIs are available to be used. However, preference should be given
to the APIs available on the [`jest` object](https://facebook.github.io/jest/docs/en/jest-object.html)

[`expect()` APIs](https://facebook.github.io/jest/docs/en/expect.html)

For React component testing, [react-testing-library](https://github.com/kentcdodds/react-testing-library) is preferred.

Example:

```javascript
it('should render a title property', () => {
  const {getByText} = render(
    <MemoryRouter>
      <PropertyFinancialSummaryComponent title={'Financial Summary'} />
    </MemoryRouter>
  );
  getByText('Financial Summary');
});
```

See the [docs](https://github.com/kentcdodds/react-testing-library#usage) for more details

### Coverage Reporting

Jest has an integrated coverage reporter that works well with ES6 and requires no configuration.<br>
Run `npm test -- --coverage` (note extra `--` in the middle) to include a coverage report like this:

![coverage report](http://i.imgur.com/5bFhnTS.png)

Note that tests run much slower with coverage so it is recommended to run it separately from your normal workflow.

#### Configuration

The default Jest coverage configuration can be overridden by adding any of the following supported keys to a Jest config in your package.json.

Supported overrides:

- [`collectCoverageFrom`](https://facebook.github.io/jest/docs/en/configuration.html#collectcoveragefrom-array)
- [`coverageReporters`](https://facebook.github.io/jest/docs/en/configuration.html#coveragereporters-array-string)
- [`coverageThreshold`](https://facebook.github.io/jest/docs/en/configuration.html#coveragethreshold-object)
- [`snapshotSerializers`](https://facebook.github.io/jest/docs/en/configuration.html#snapshotserializers-array-string)

Example package.json:

```json
{
  "name": "your-package",
  "jest": {
    "collectCoverageFrom": [
      "src/**/*.{js,jsx}",
      "!<rootDir>/node_modules/",
      "!<rootDir>/path/to/dir/"
    ],
    "coverageThreshold": {
      "global": {
        "branches": 90,
        "functions": 90,
        "lines": 90,
        "statements": 90
      }
    },
    "coverageReporters": ["text"],
    "snapshotSerializers": ["my-serializer-module"]
  }
}
```

module.exports = {
  bracketSpacing: false,
  singleQuote: true,
  jsxBracketSameLine: true,
  trailingComma: 'es5',
  arrowParens: 'always',
  printWidth: 90,
  overrides: [
    {
      files: '*.scss',
      options: {
        parser: 'css',
      },
    },
  ],
};

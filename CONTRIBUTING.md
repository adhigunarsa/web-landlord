# Contributing to web-tenant

## Setup

web-tenant requires Node >=10 and NPM >=6

```
npm install
npm start
# Open http://web.local.lodg.io
```

## Dev tooling in web-frontend

All commands are managed as npm scripts. These commands are always run from the project root.

Below are the most frequently used commands, please look at the root package.json file for more.

### Running

```bash
# To start a local dev server

npm start
# Opens http://web.local.lodg.io automatically

# To start a local Storybook instance
npm run storybook
# Open http://localhost:6006
```

### Testing

See [testing.md](docs/testing.md) for more details on unit testing in web-frontend

```bash
# To run tests in watch mode
npm test

# To run tests with chrome dev tools debugger attached
npm run test:debug
```

### Code formatting and linting

```bash
# To reformat TS, JS, JSON, SCSS styles and Markdown docs sources
npm run prettier

# To lint sources
npm run lint
```

## Git Branching

All changes should be branched off the master branch. All feature branches must be rebased on master before they can
be merged.

### Code review and merging.

To request a review, raise a Merge Request in Gitlab.

The title should be written in the conventional commit format (see below for the format).

Merge requests can be raised early for feedback while development is ongoing. Set the title to
`WIP(<scope>): <short description>` and update the title when ready to be merged.

All commits that are merged to master are to be squashed. The Gitlab UI allows the author to check a box when raising
the MR to squash the commits when the MR is merged. Ensure that the final commit message follows the conventional commit format.

## Commit messages

All commit messages are required to follow the [Conventional Commits]([https://conventionalcommits.org) convention.

### Format

The commit format is as follows:

```
<type>[optional scope]: <description>

[body]

Related Jira issues: <Jira ticket ids>
```

Where type is one of

- feat: A new feature
- fix: A bug fix
- docs: Documentation only changes
- refactor: A code change that neither fixes a bug nor adds a feature
- perf: A code change that improves performance
- test: Adding missing tests
- chore: Changes to the build process or auxiliary tools
- revert: Revert to a commit

### Why

Conventional Commits is used to keep the git history meaningful. The convention requires describing
commit messages in terms of features, fixes, and breaking changes, which follows the [SemVer](http://semver.org/) standard.

## Further docs

The [docs](docs) folder contains additional project docs.

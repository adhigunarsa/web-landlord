/* eslint-disable no-console */
// Based on https://github.com/facebook/react/blob/master/scripts/prettier/index.js

const chalk = require('chalk');
const glob = require('glob');
const prettier = require('prettier');
const fs = require('fs');
const listChangedFiles = require('../git/listChangedFiles');
const prettierConfigPath = require.resolve('../../.prettierrc');

const mode = process.argv[2] || 'check';
const shouldWrite = mode === 'write' || mode === 'write-changed';
const onlyChanged = mode === 'check-changed' || mode === 'write-changed';

const changedFiles = onlyChanged ? listChangedFiles() : null;
let didWarn = false;
let didError = false;

const files = glob
  .sync('**/*.?(md|js|jsx|json|ts|tsx|scss|yml|yaml)', {
    ignore: [
      '**/node_modules/**',
      '**/local_modules/**',
      '**/coverage/**',
      '**/build/**',
      '**/storybook-static/**',
    ],
  })
  .filter((f) => !onlyChanged || changedFiles.has(f));

if (!files.length) {
  return;
}

files.forEach((file) => {
  const options = prettier.resolveConfig.sync(file, {
    config: prettierConfigPath,
  });
  try {
    const input = fs.readFileSync(file, 'utf8');
    if (shouldWrite) {
      const output = prettier.format(input, {...options, filepath: file});
      if (output !== input) {
        fs.writeFileSync(file, output, 'utf8');
      }
    } else {
      if (!prettier.check(input, {...options, filepath: file})) {
        if (!didWarn) {
          console.log(
            '\n' +
              chalk.red(
                `  This project uses prettier to format all Typescript, JavaScript, Markdown, JSON, SCSS code.\n`
              ) +
              chalk.dim(`    Please run `) +
              chalk.reset('npm run prettier:all') +
              chalk.dim(` and add changes to files listed below to your commit:`) +
              `\n\n`
          );
          didWarn = true;
        }
        console.log(file);
      }
    }
  } catch (error) {
    didError = true;
    console.log('\n\n' + error.message);
    console.log(file);
  }
});

if (didWarn || didError) {
  process.exit(1);
}

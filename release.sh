#!/usr/bin/env bash

if [[ -z "${RELEASE}" ]]; then
    echo "Skipping release. Trigger a pipeline with RELEASE set to trigger a release build"
else
    echo "${RELEASE}"
    npx semantic-release
fi
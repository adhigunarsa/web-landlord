# Changelog

All changes are recorded on the [Gitlab tags page](https://gitlab.com/lodg/web-tenant/tags)
